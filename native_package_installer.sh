#!/usr/bin/env bash

## @file native_package_installer.sh
## @brief Install given packages using the native package manager
##
## The idea behind this file is to have a wrapper around the package manager of
## the underlying system to install packages from different scripts.
##
## Package names will be provided as they are in Arch Linux / AUR. If required,
## a translation table may be used to convert the name to one recognized by the
## native package manager.
##
## @author Darshit Shah <darnir@gmail.com>


# Prevent the script from being executed. It must only be sourced
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
	echo >&2 "This is a shell library script." \
	"It makes no sense to execute it directly."
	exit 1
fi

_install_package() {
	# Fast exit path
	if [[ "$#" -eq 1 ]]; then
		return 0
	fi

	distro="$(distro_detect)"
	case $distro in
		arch) _npi_arch "$@";;
		fedora) _npi_fedora "$@";;
		*) echo "Distro $distro not yet supported" >&2
			exit 50;;
	esac
	return $?
}

_npi_fedora() {
	local dnf_install_option=""
	local pkgs=()
	local no_inst=()
	shift
	for pkg in "$@"; do
		case "$pkg" in
			diff-so-fancy) no_inst+=( $pkg );;
			*) pkgs+=( $pkg );;
		esac
	done

	if [[ ${#no_inst[@]} -gt 0 ]]; then
		INFO "Following packages will not be installed: ${no_inst[*]}"
	fi

	if [[ ${#pkgs[@]} -gt 0 ]]; then
		TRACE "Installing (dnf): ${pkg[*]}"
		sudo dnf install $dnf_install_option "${pkgs[@]}"
	fi
	return $?
}

_npi_arch() {
	local pacman_install_option=""
	if [[ "$1" == "false" ]]; then
		pacman_install_option="--asdeps"
	fi

	shift

	# yay is currently our AUR wrapper of choice.
	# If it does not exist, simply pass everything to pacman. It will fail and
	# the user must handle the installation manually.
	if command -v yay &>/dev/null; then
		yay -S $pacman_install_option "$@"
	else
		sudo pacman -S $pacman_install_option "$@"
	fi
	return $?
}

## @fn distro_detect()
## Echo back the ID string of the distro.
##
## Currently, this function only tests for the `ID` variable in
## `/etc/os-release`. However, if required, it can be extended to support other
## means of identification as well.
##
## The values returned are:
## Distro Name | ID
## ------------|---
## Arch Linux  | arch
## Fedora      | fedora
distro_detect() {
	dist="$(grep "^ID=" /etc/os-release 2>/dev/null | cut -d= -f2)"
	echo "${dist:-unknown}"
}

install_native_dependency() {
	# Is manuallyInstalled=false
	_install_package false "$@"
}

install_native_package() {
	_install_package true "$@"
}
