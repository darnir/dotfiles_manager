# Bash Dotfiles Manager

TESTS_DIR=tests

.PHONY: doc check build_doc

all: doc

doc: build_doc

# Doesn't currently work due to UTF-8 sequences
build_pdf:
	$(MAKE) -C doc/latex

build_doc:
	@doxygen

check:
	$(MAKE) -C $(TESTS_DIR)

clean:
	$(RM) -rf docs/*
