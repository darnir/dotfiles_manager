#!/usr/bin/env bash

test_description="Test the _get_dest() routine"

. sharness.sh

set -e

unset XDG_CONFIG_HOME
unset XDG_BIN_HOME

# shellcheck source=init_library.sh
. "${SHARNESS_TEST_SRCDIR}/init_library.sh"

test_expect_success "Test get_dest" "echo /etc/pacman.conf > exp &&
	_get_dest pacman.conf sysconf &&
	echo \"\${_dot_lib_dest}\" > dest &&
	test_cmp exp dest"

test_done

# vim: set ts=4 sts=4 sw=4 tw=79 ft=sh noet :
