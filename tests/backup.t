#!/usr/bin/env bash

test_description="Test creating file backups"

. sharness.sh

set -e

# shellcheck source=init_library.sh
. "${SHARNESS_TEST_SRCDIR}/init_library.sh"

FILE_CONTENTS="Test File"

check_file() {
	if [[ ! -f "$1" ]]; then
		exit 11
	fi

	cmp "$1" - <<< "${2:-$FILE_CONTENTS}"
}

check_symlink() {
	if [[ ! -f "$1" ]]; then
		exit 11
	fi

	cmp "$1" - <<< "$FILE_CONTENTS"
}

check_num_files() {
	d=$(ls | wc -l)
	if [[ $d -ne $1 ]]; then
		exit $((d+20))
	fi
}

test_expect_success "file backup - Non-Existing File" "_backup_target \"$PWD/nofile\" &&
	test_when_finished check_num_files 0"

test_expect_success "file backup - Basic" "echo 'Test File' > file.txt &&
	_backup_target \"$PWD/file.txt\" &&
	test_when_finished check_num_files 1 &&
	test_when_finished check_file file.txt.bck"


test_expect_success "file backup - Existing File" "echo Test2 > file.txt &&
	_backup_target \"$PWD/file.txt\" &&
	test_when_finished check_num_files 2 &&
	test_when_finished check_file file.txt.bck.1 Test2"

test_expect_success "file backup - symlink" "ln -s file.txt.bck newfile.txt &&
	_backup_target \"$PWD/newfile.txt\" &&
	test_when_finished check_num_files 3 &&
	test_when_finished check_symlink newfile.txt.bck"

check_dirtest() {
	if [[ -d "tdir" ]]; then
		exit 10
	fi

	cmp tdir_bck/file.txt <<< "$FILE_CONTENTS"
}

test_expect_success "dir backup" "mkdir tdir &&
	echo \"$FILE_CONTENTS\" > tdir/file.txt &&
	_backup_target \"$PWD/tdir\" &&
	test_when_finished check_dirtest"

test_done

# vim: set ts=4 sts=4 sw=4 tw=79 ft=sh noet :
