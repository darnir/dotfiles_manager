#!/usr/bin/env bash

test_description="Test the db functions"

. sharness.sh

set -e

. "${SHARNESS_TEST_SRCDIR}/init_library.sh"


db_expect="\\# Dotfiles Install Database"

test_expect_success "test init_db" "echo $db_expect > expected &&
	init_db &&
	test_cmp expected test.db"

test_done

# vim: set ts=4 sts=4 sw=4 tw=79 ft=sh noet :
