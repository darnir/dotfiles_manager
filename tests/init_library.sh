#!/usr/bin/env bash

if [[ -z $SHARNESS_BUILD_DIRECTORY ]]; then
	exit 1
fi

source "$SHARNESS_BUILD_DIRECTORY/b-log/b-log.sh"
source "$SHARNESS_BUILD_DIRECTORY/dotfiles_library.sh"

export DBFILE="test.db"
