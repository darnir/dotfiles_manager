#!/usr/bin/env bash

test_description="Ensure Documentation"

. sharness.sh

#shellcheck source=../dotfiles
. "${SHARNESS_BUILD_DIRECTORY}/dotfiles"

set +u

test_help() {
	local h_output
	h_output="$("${SHARNESS_BUILD_DIRECTORY}/dotfiles" -h)"
	while read -n1 char; do
		if [[ "$char" == ":" ]]; then
			continue
		elif ! grep -q "\\-$char" <<< "$h_output"; then
			return 1
		fi
	done <<<$OPTSTRING
}

export -f test_help

test_expect_success "Test Help Strings" "test_help"

test_done
