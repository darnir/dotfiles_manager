#!/usr/bin/env bash

## @file dotfiles_library.sh
## @brief Library functions for dotfiles script
##
## This file contains various helper functions for the dotfiles script
## This script *MUST* be sourced in the main script after the logging has been
## initialized
##
## @author Darshit Shah <darnir@gmail.com>


# Prevent the script from being executed. It must only be sourced
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
	echo >&2 "This is a shell library script." \
	"It makes no sense to execute it directly."
	exit 1
fi

if [[ "$(type -t B_LOG)" != "function" ]]; then
	echo "The library script must be sourced after the logging framework" >&2
	exit 1
fi

# Pointer to the dotfiles installation database
DBFILE="$SCRIPT_DIR/dotfiles.db"

## The location where files will be installed location `userconf`
declare USERCONF=${XDG_CONFIG_HOME:-$HOME/.config}
## The location where files will be installed location `sysconf`
declare SYSCONF=${SYSCONFDIR:-/etc}
## The location where files will be installed location `bin`
declare USERBIN=${XDG_BIN_HOME:-$HOME/bin}

###############################################################################
# Private Helper Functions

## @fn _begin_install()
## Internal: Helper function that is called before any file is installed.
## Add any preamble / set up code here
_begin_install() {
	INFO "Installing File $1"
	trap _end_install EXIT
}

## @fn _end_install()
## Internal: Cleanup / teardown function after a package is installed.
_end_install() {
	last_stat=$?
	if [[ $last_stat -eq 0 ]]; then
		TRACE "Installation Successful"
	else
		ERROR "Installation Failed. Exit Code: $last_stat"
	fi
	trap - EXIT
}

## @fn _backup_target()
## Internal: Backup a given target location
## \param[in] target. The target location which should be backed up
## \param[in] sudo. The privilege escalation command to use (optional)
##
## This function accepts a parameter as `$1` and creates a backup copy.
##
## If the target location is a symlink, then it will traverse the symlink
## and create a copy of the location that it points to.
##
## The name of the backup file depends on the type of the target after
## traversing any symlinks:
##   * **File**: A `.bck` is appended to the filename. E.g.: backup.txt.bck
##   * **Directory**: A `_bck` is appended. E.g.: backupdir_bck
##
## If a file already exists with the name to which the backup is to be created,
## then, append a numeric identifier to the name so as to make it unique.
_backup_target() {
	local target="$1"
	local SUDO_REQ="${2:-}"

	TRACE "Checking if $target already exists"

	# Early exit if no file to backup
	if [ ! -e "$target" ] && [ ! -L "$target" ]; then
		TRACE "Do not need a backup"
		return 0
	fi

	DEBUG "$target already exists. Backing up"

	local ext
	local real_target
	real_target="$(readlink -m "$target")"

	if [ ! -e "$real_target" ]; then
		TRACE "$target is a broken symlink. Removing without backup"
		unlink "$target"
		return 0
	fi

	if [[ -f "$real_target" ]]; then
		ext="."
	else
		ext="_"
	fi

	local backupname="${target}${ext}bck"
	local backupsave="$backupname"
	local bck_count=1

	# If the backup file exists as well, count up to find a name that does
	# not exist
	while [ -e "${backupname}" ]; do
		backupname="${backupsave}${ext}${bck_count}"
		bck_count=$((bck_count+1))
	done

	DEBUG "Saving original file to $backupname"

	if [[ -L "$target" ]]; then
		TRACE "Target is Symlink. Copying..."
		${SUDO_REQ} cp -R "${real_target}" "${backupname}"
		${SUDO_REQ} unlink "$target"
	else
		${SUDO_REQ} mv "${real_target}" "${backupname}"
	fi

	DEBUG "Backup successful"
}

## @fn _get_dest()
## Interpret the location keywords into an on-disk location.
##
## The location is one of the following special
## values that identifies the correct location on the system:
##  * userconf: This maps to `$XDG_CONFIG_HOME`
##  * userpkg:  This maps to `$XDG_CONFIG_HOME/$PACKAGE_NAME`
##  * sysconf:  This maps to `/etc/`
##  * bin:      This maps to `$XDG_BIN_HOME`
##  * home-hidden: This maps to `$HOME/`. But the file will be hidden
##
## If anything is specified after the location keyword, then that is appended
## to the generated destination above. This is useful for putting configuration
## files into a specific directory within the precribed location.
##
## _WARNING: The directory parameter when applied to `home-hidden` keyword will
## cause the file to be installed in a hidden directory in `$HOME`, but the file
## itself will not be hidden._
_get_dest() {
	local file_name="$1"
	local location="$2"
	local dest

	case $location in
		userconf) dest="$USERCONF/";;
		userpkg) dest="$USERCONF/${PACKAGE_NAME}/";;
		sysconf) dest="$SYSCONF/";;
		bin) dest="$USERBIN/";;
		home-hidden) dest="$HOME/."
			WARN "$file_name is installed to \$HOME as a hidden file.";;
		*) dest="$1/";;
	esac

	TRACE "Install Destination: $dest"

	# Create the final filename.
	# If a custom directory is supplied, apply it to the computed path first
	local append_dir=""
	if [[ -n ${3:-} ]]; then
		append_dir="$3/"
	fi

	dest="${dest}${append_dir}$file_name"

	TRACE "Final Install Destination: $dest"

	_dot_lib_dest="$dest"
}

_real_install() {
	local install_command="$1" && shift

	_begin_install "$@"
	local file_name="$1"
	local dest

	_get_dest "$@"
	dest="${_dot_lib_dest}"

	# If the destination is anywhere outside of $HOME, we assume that we need
	# superuser privileges to access it.
	local SUDO=""
	if [[ ! "$dest" =~ $HOME/* ]]; then
		SUDO="sudo"
	fi

	DEBUG "Sudo command: \"$SUDO\""

	# Create the path to the file if it does not already exist
	$SUDO mkdir -p "$(dirname "$dest")"

	local source
	source="$PKGDIR/$file_name"
	TRACE "Checking if $source exists"
	if [[ ! -e "$source" ]]; then
		ERROR "$file_name was not found in the Package"
		exit 1
	fi

	_backup_target "$dest" "$SUDO"

	DEBUG "Linking $source -> $dest"

	TRACE "$SUDO $install_command $source $dest"
	# shellcheck disable=SC2086
	$SUDO $install_command "$source" "$dest"

	_end_install
}

###############################################################################
# Public Interface

## @fn init_db()
## Initialize the dotfiles database file.
init_db() {
	if [[ ! -f "$DBFILE" ]]; then
		TRACE "Initializing a new database"
		echo "# Dotfiles Install Database" > "$DBFILE"
	fi
}

log_installed() {
	pkg="$1"
	echo "$pkg" >> "$DBFILE"
}

## @fn copy_file()
## Copy a file to the given location
## \param[in] filename. The name of the file to install
## \param[in] location. Location on system to which to install
## \param[in] target_dir. The directory within the `location` (optional)
##
## Sometimes, one wants to track a file that is modified by an external
## utility. Some such utilities will often delete the file and completely rewrite
## it for an update. As a result, if the file was a symlink, the link will be
## deleted and the repository will no longer track it. This is why we allow one to
## simply copy the file to the target location. The packages respository has a
## _post-commit_ git hook which can be installed to track changes to such files
## and inform the user.
copy_file() {
	TRACE "COPY file: $*"
	_real_install "cp -R" "$@"
}

## @fn install_file()
## Install a given dotfile to a particular location
## \param[in] filename. The name of the file to install
## \param[in] location. Location on system to which to install
##
## This function accepts a filename as `$1` and installs it to the location
## specified in `$2`. The location is a keyword which is interpreted according
## to `_get_dest()`.
##
## By default, if the target is not in `$HOME`, then privilege escalation using
## `sudo` is used. In order to override this behaviour, set the `$AC_SUDO`
## variable with the name of the command to use, or to empty.
install_file() {
	TRACE "LINK file: $*"
	_real_install "ln -s" "$@"
}

## @fn distro_id()
## Echo back the ID string of the distro.
##
## Currently, this function only tests for the `ID` variable in
## `/etc/os-release`. However, if required, it can be extended to support other
## means of identification as well.
##
## The values returned are:
## Distro Name | ID
## ------------|---
## Arch Linux  | arch
##
## _DEVEL: Never use logging within this function. It must always print
## **ONLY** the distro id to screen and nothing else_
distro_id() {
	dist="$(grep "ID=" /etc/os-release 2>/dev/null | cut -d= -f2)"
	echo "${dist:-unknown}"
}

run_pkgbuild() {
	if [[ ! "$(distro_id)" == "arch" ]]; then
		WARN "The current system is not Arch Linux. Skipping PKGBUILD"
		return
	fi

	if [[ -f "$PKGDIR/PKGBUILD" ]]; then
		local curdir
		curdir="$(pwd)"
		cd "$PKGDIR" || return
		makepkg -si
		cd "$curdir" || return
	fi
}


# vim: set ts=4 sts=4 sw=4 tw=79 noet :
