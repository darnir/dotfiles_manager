# Dotfiles Manager

`Dotfiles Manager` is a way of managing all your dotfiles on a \*nix
system with ease. This project spawned from the simple idea of writing a couple
of scripts to automate the task of "installing" the configuration files to the
right locations on a new machine. It was later extended to use the concept of
packages to provide a finer granularity on which configuration files are
installed on a new system. Configuration files which should ideally all be
installed together, are grouped together as a single package. As of date, most
of the configuration files now live in a single location, making the idea of
needing a helper to set up the files redundant. However, I very much like the
convenience that this provides in automating a new setup.

When trying to keep a set of configuration files in a separate repository, it is
important that the repository be able to automatically track the changes to the
file. We should not have to remember to copy the file to the repository first.
Hence, the simplest way to accomplish this is to keep all the configuration
files in a single location which is tracked under a version control system. From
this location, the files are symlinked to the specific locations in the
filesystem where the relevant programs expect to find them. In this project, we
use Git as the version control system.

This project works by creating a symlink for the file from the Package in the
repository to the location on the system where it needs to be. As a result, any
changes to the configuration files, AFTER the package has been installed will
automatically be reflected in the repository. Since this is a Git repository,
one can look at the diffs of the changes and revert back to the original state
if need be. Upon confirming the changes are good, they should be immediately
committed to the repository.

A lot of the design decisions in this project are inspired from Arch Linux's
pacman and the format of the PKGBUILD files for defining a package.

## Packages

A package is a collection of files which configure a particular aspect of the
system. Hence, one can have a package for Vim, Bash, SSH, X, etc. To create a
new package, simply create a new directory with the package name within the
`Packages/` directory and populate it with the files and directories that will
need to be tracked within this framework. Then write an `InstallScript` file
which contains the mappings for where each of the files in the packages should
be installed on the target system.

All packages live in a directory called "Packages" in the root of this git
directory.

### InstallScripts

The concept of an `InstallScript` is based on ArchLinux's PKGBUILDs. An
`InstallScript` is a valid Bash script which is executed to install a particular
package. The script must implement the following variables:

  * **`PACKAGE_NAME`**: This is the name of the package being installed. It
    **must** contain the same name as that of the directory in which it resides.
    This variable is used to track Package Dependencies currently, though it may
    be used for other things in the future.

The master `dotfiles` script will invoke the following functions in the
specified order from the `InstallScript`. Only the `install()` function is
mandatory, the others may not be defined if there is no requirement.

   1. **`prepare()`**: Used to perform initialization tasks before a package is
      installed. This is includes installing dependencies, creating the
      directory structure, setting environment variables, etc.
   2. **`install()`**: It is used for the actual installation of the files
      in the system.
   3. **`post_install()`**: Used to perform some post-installation tasks like
      setting the attributes of a file, or installing files not included in
      dotfiles.

Similar to a PKGBUILD, one may also specify package dependencies. There are two
types of dependencies:

   1. `dotdepends`: The `dotdepends` is declared a Bash array and is a list of
      other dotfiles packages which are a dependency for this package. Ideally,
      you want only hard-dependencies here and print out any optional
      dependencies using the `post_install` function.
   2. `depends`: This too is a Bash array that lists the packages that need to
      be installed on the system for these dotfiles

One example of using these dependencies is to create *meta packages*. A meta
package installs no files on its own, but instead is a collection of
dependencies which can all be installed in a single command. For example, I use
the following package layout:

```
.
├── neomutt
├── private
│   ├── Mail
│   │   ├── getmail
│   │   ├── msmtp
│   │   ├── neomutt
│   │   ├── offlineimap
│   │   └── procmail
```

Here, installing `private/Mail` will automatically install and have my entire
mail handling set of applications configured.

It is also possible to make some interesting layouts. I have my dotfiles in a
public repository, but sometimes there is some information which I would rather
not make public. Such files go into a separate "private/" directory which is
simply a second repository that is not publicly available. My `neomutt` package
does this. The basic configuration is public, but a list of all mailing lists
that I'm subscribed to is a part of the private repository.

"Installing" a file is the act of creating a symlink from the
file in the repository to the path on the system where the configuration file is
expected to exist. For example, the `bashrc` file is generally expected to
exist at `~/.bashrc`, hence installing the file is equivalent to running the
command `$ ln -s $PWD/Packages/Bash/bashrc $HOME/.bashrc` from the root of the
repository. To make things easier, the package manager provides some helper
functions that can be utilized for installing files and directories.

Usage
-----

The central entity in this project is the `dotfiles` script available in the
root directory of the repository. The script assumes that the user actually
knows what he/she is doing and as a result does not implement many sanity
checks. However, with a little bit of common, using this script shouldn't pose
any problems at all. One of the chief limitations of the script is that it
must always be called from the root directory of the repository
since it uses relative paths from there. Since I don't care much about this
restriction, I've decided not to work on it. However, if someone submits a
patch, I would not mind merging it.

```
Usage: ./dotfiles [Options]
List of valid options:
-S package  : Install dotfiles package "package"
-l          : List available packages
-h          : Print this help and exit
Remember, this script must only be invoked from the root dir of the repository
```

## Requirements

The dotfiles manager itself requires only `bash>4.0`.

This project is tested using [sharness](https://github.com/chriscool/sharness)
and the documentation is generated using [Doxygen](https://doxygen.org) and
[bash-doxygen](https://github.com/Anvil/bash-doxygen). Both the projects are
included in this repository as a git submodule.

## TODO

  1. Before backing up a file, allow the user to immediately merge the two
  2. When installing a package, test if it has already been installed
  3. Do not re-install a dependency package if it has already been installed
  4. Before installing a file, try to figure out if the `ln` command will
     succeed. The most common failure is creating hard links across devices.
  5. Support optional dependencies like PKGBUILD does
  6. Support dependency handling for non-Arch systems

## License
MIT

## Authors
Darshit Shah  <darnir@gmail.com>

>  vim: set ts=2 sts=4 sw=2 tw=80 et :
